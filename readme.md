# Make your editors happy - Codegarden 15 #

This repository contains the demo website from my Codegarden talk called "Make your editors happy"

To get this running locally take the following step :

1. Clone the repository to your local machine
2. Open the solution in Visual Studio and build it. Nuget restore is enabled so all dependencies will get downloaded on first build
3. [Download](https://bitbucket.org/dawoe/codegarden-15/downloads/codegarden15.zip) the backup of the database and restore it on your database server (SQL Server 2012). The credentials can be found in the web.config
4. Update your web.config to match your database server
5. Run the create-iis-site.bat file. This will set up IIS for you.
6. Open a browser and go to http://codegarden15.local/umbraco
7. You can log in using admin/admin123


The slides of the presentation can be found [here](https://bitbucket.org/dawoe/codegarden-15/downloads/Make%20your%20editors%20happy%20-%20Codegarden%2015.pdf).

