﻿using System;
using System.Collections.Generic;

namespace Codegarden15.UrlSegmentProvider
{
    using Umbraco.Web;
    using Umbraco.Web.Routing;

    public class VortoUrlProvider : IUrlProvider
    {
        public string GetUrl(UmbracoContext umbracoContext, int id, Uri current, UrlProviderMode mode)
        {
            return umbracoContext.ContentCache.GetById(id).Url;
        }

        public IEnumerable<string> GetOtherUrls(UmbracoContext umbracoContext, int id, Uri current)
        {
            throw new NotImplementedException();
        }
    }
}