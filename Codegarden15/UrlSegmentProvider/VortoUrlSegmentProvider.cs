﻿namespace Codegarden15.UrlSegmentProvider
{
    using System;
    using System.Globalization;

    using Umbraco.Core;
    using Umbraco.Core.Models;
    using Umbraco.Core.Strings;

    public class VortoUrlSegmentProvider : IUrlSegmentProvider
    {
        public string GetUrlSegment(IContentBase content)
        {
            return this.GetUrlSegment(content, new CultureInfo("en-US"));
        }

        public string GetUrlSegment(IContentBase content, CultureInfo culture)
        {
            var ctAlias = this.GetContentTypeAlias(content);

            if (!ctAlias.Equals("MultilanguagePage"))
            {
                return null;
            }

            string urlSegment = content.Name.ToUrlSegment();

            return urlSegment;
        }

        private string GetContentTypeAlias(IContentBase content)
        {
            try
            {
                var ct = ApplicationContext.Current.Services.ContentTypeService.GetContentType(
                    content.ContentTypeId);

                return ct.Alias;
            }
            catch (Exception e)
            {
                return string.Empty;
            }
        }
    }
}