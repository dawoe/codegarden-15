REM Define variables
set hostspath=%windir%\System32\drivers\etc\hosts
set appcmdpath=%windir%\System32\inetsrv
set sitedir=%CD%\Codegarden15
set project=codegarden15
REM Delete site and pool
%appcmdpath%\appcmd.exe delete site /site.name:"%project%"
%appcmdpath%\appcmd.exe delete apppool /apppool.name:"%project%"

REM Create AppPool
%appcmdpath%\appcmd.exe add apppool /name:"%project%"
%appcmdpath%\appcmd.exe set apppool /apppool.name:"%project%" /processModel.identityType:NetworkService
%appcmdpath%\appcmd.exe set apppool /apppool.name:"%project%" /enable32BitAppOnWin64:False
%appcmdpath%\appcmd.exe set apppool /apppool.name:"%project%" /managedPipelineMode:Integrated
%appcmdpath%\appcmd.exe set apppool /apppool.name:"%project%" /managedRuntimeVersion:v4.0
%appcmdpath%\appcmd.exe set apppool /apppool.name:"%project%" /autoStart:True
%appcmdpath%\appcmd.exe start apppool /apppool.name:"%project%"

REM Create site
%appcmdpath%\appcmd.exe add site /name:"%project%" /physicalPath:"%sitedir%"
%appcmdpath%\appcmd.exe set app /app.name:"%project%/" /applicationPool:"%project%"

REM Define bindings (leave first as)
%appcmdpath%\appcmd set site /site.name:"%project%" /+bindings.[protocol='http',bindingInformation='*:80:codegarden15.local']
%appcmdpath%\appcmd set site /site.name:"%project%" /+bindings.[protocol='http',bindingInformation='*:80:www.codegarden15.local']

REM start site
%appcmdpath%\appcmd.exe start site /site.name:"%project%"

@echo off
REM Add hosts to host file
echo. >> %hostspath%
echo 127.0.0.1		codegarden15.local >> %hostspath%
echo 127.0.0.1		www.codegarden15.local >> %hostspath%